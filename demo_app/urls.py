from django.urls import path
from . import views

urlpatterns = [
    path('', views.username, name='home-page'),
    path('users', views.userList.as_view()),
]